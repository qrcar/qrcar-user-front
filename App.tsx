// @ts-ignore
import React, {FC, useEffect, useState} from 'react';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {Text, View} from 'react-native';
import {AuthNavigator, MainNavigator} from './src/components/navigation';

const App: FC = () => {
  const myTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'rgb(255,255,255)',
    },
  };

  return (
    <>
      <NavigationContainer>
        <MainNavigator />
      </NavigationContainer>
    </>
  );
};

export default App;

enum Variant {
  royalblue = '#4169e1',
  accent = '#F3534A',
  primary = '#A476EF',
  primaryDark = '#7728FA',
  secondary = '#2BDA8E',
  tertiary = '#FFE358',
  black = '#323643',
  white = '#FFFFFF',
  darkgray = '#999998',
  gray = '#9DA3B4',
  gray2 = '#C5CCD6',
  gray3 = '#ebebeb',
  orange = '#ffa232',
}

export interface IVariant {
  royalblue?: boolean;
  accent?: boolean;
  primary?: boolean;
  secondary?: boolean;
  tertiary?: boolean;
  black?: boolean;
  white?: boolean;
  darkgray?: boolean;
  gray?: boolean;
  gray2?: boolean;
  gray3?: boolean;
  orange?: boolean;
  primaryDark?: boolean;
}

export default Variant;

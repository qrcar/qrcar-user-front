interface Address {
  postalCode: string;
  city: string;
  addressLine_1: string;
  addressLine_2?: string;
}

export default Address;

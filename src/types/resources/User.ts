interface User {
  id?: string;
  lastname: string;
  email: string;
  firstname: string;
}

export default User;

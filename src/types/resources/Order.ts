import User from './User';
import Establishment from './Establishment';
import {Card} from './Card';

export enum Status {
  waiting = 'waiting',
  paid = 'paid',
  refund = 'refund',
  delivered = 'delivered',
  canceled = 'canceled',
}

export enum PaymentMethod {
  paypal = 'paypal',
  creditCard = 'creditCard',
}

interface Order {
  id?: string;
  cost: string;
  order_num: string;
  user_id: User['id'];
  status: Status;
  establishment_id: Establishment['id'];
  paymentMethod: PaymentMethod;
  date: string;
  card_id?: Card['card_id'] | null;
}

export default Order;

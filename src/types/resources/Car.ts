import User from './User';

interface Car {
  id: string;
  user_id: User['id'];
  plateRegistration: string;
}

export default Car;

import Address from './Address';

interface Establishment {
  id?: string;
  siret: string;
  address: Address;
  phone: string;
  name: string;
}

export default Establishment;

import User from './User';

interface Card {
  id: string;
  card_id: string;
  user_id: User['id'];
}

export default Card;

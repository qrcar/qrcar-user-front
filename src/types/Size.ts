enum Size {
  // global sizes
  base = 16,
  font = 14,
  radius = 6,
  padding = 25,

  // font sizes
  h0 = 32,
  h1 = 26,
  h2 = 20,
  h3 = 18,
  title = 20,
  header = 16,
  body = 14,
  caption = 12,
  small = 11,
}

export interface ISize {
  h0?: boolean;
  h1?: boolean;
  h2?: boolean;
  h3?: boolean;
  title?: boolean;
  header?: boolean;
  body?: boolean;
  caption?: boolean;
  small?: boolean;
}

export default Size;

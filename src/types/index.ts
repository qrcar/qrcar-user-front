export type {default as Establishment} from './resources/Establishment';
export type {default as Order} from './resources/Order';
export type {default as User} from './resources/User';

export type {ISize} from './Size';
export type {IVariant} from './Variant';

export {default as Variant} from './Variant';
export {default as Size} from './Size';

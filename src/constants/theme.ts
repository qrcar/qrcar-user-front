import {Size} from '../types';

const fonts = {
  h0: {
    fontSize: Size.h0,
  },
  h1: {
    fontSize: Size.h1,
  },
  h2: {
    fontSize: Size.h2,
  },
  h3: {
    fontSize: Size.h3,
  },
  header: {
    fontSize: Size.header,
  },
  title: {
    fontSize: Size.title,
  },
  body: {
    fontSize: Size.body,
  },
  caption: {
    fontSize: Size.caption,
  },
};

export {fonts};

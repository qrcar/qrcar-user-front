// @ts-ignore
import React, {FC} from 'react';
import {useNavigation} from '@react-navigation/native';
import LayoutBase from '../../components/layout/statusbar-layout/LayoutBase';
import {StarterTopPart} from '../../components/section';
import {LoginForm} from '../../components/forms';
import {Block, Button, Text} from '../../components/element';
import Size from '../../types/Size';
import Variant from '../../types/Variant';

const LoginScreen: FC = () => {
  const navigation = useNavigation();
  return (
    <LayoutBase>
      <Block>
        <StarterTopPart />
      </Block>
      <Block margin={[0, Size.base * 2]}>
        <Text bold size={Size.title}>
          Connexion
        </Text>
        <LoginForm />
        <Button
          size={Size.base}
          margin={[20, 0]}
          bold
          onPress={() => navigation.navigate('Register')}
          center
          color={Variant.primary}>
          Crée un compte
        </Button>
      </Block>
    </LayoutBase>
  );
};

export default LoginScreen;

// @ts-ignore
import React, {FC} from 'react';
import {TunnelLayout} from '../../components/layout';
import {CardLastSpend, CardSpend, CardSpendChart} from '../../components/card';

const DashboardScreen: FC = () => {
  return (
    <TunnelLayout statusBar title='Accueil'>
      <CardSpend value={124.36} />
      <CardSpendChart title='Dépenses par moi' />
      <CardLastSpend title='Dernières dépense' />
    </TunnelLayout>
  );
};

export default DashboardScreen;

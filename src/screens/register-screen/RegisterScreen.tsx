// @ts-ignore
import React, {FC} from 'react';
import {useNavigation} from '@react-navigation/native';
import LayoutBase from '../../components/layout/statusbar-layout/LayoutBase';
import {StarterTopPart} from '../../components/section';
import {RegisterForm} from '../../components/forms';
import {Block, Button, Text} from '../../components/element';
import Size from '../../types/Size';
import Variant from '../../types/Variant';

const RegisterScreen: FC = () => {
  const navigation = useNavigation();

  return (
    <LayoutBase>
      <Block flex={false} height={375} position='relative'>
        <StarterTopPart />
      </Block>
      <Block margin={[10, Size.base * 2, 0, Size.base * 2]} flex>
        <Text bold size={Size.title}>
          Inscription
        </Text>
        <RegisterForm />
        <Button
          size={Size.base}
          margin={[0, 0, 30, 0]}
          bold
          onPress={() => navigation.navigate('Login')}
          center
          color={Variant.primary}>
          Déjà un compte ?
        </Button>
      </Block>
    </LayoutBase>
  );
};

export default RegisterScreen;

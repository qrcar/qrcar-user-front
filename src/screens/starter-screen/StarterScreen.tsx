// @ts-ignore
import React, {FC} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Block, Button, Text} from '../../components/element';
import LayoutBase from '../../components/layout/statusbar-layout/LayoutBase';
import {StarterTopPart} from '../../components/section';
import Size from '../../types/Size';
import Variant from '../../types/Variant';

const StarterScreen: FC = () => {
  const navigation = useNavigation();

  return (
    <LayoutBase>
      <Block>
        <StarterTopPart />
      </Block>
      <Block flex space='between' margin={[0, Size.base * 2]}>
        <Block flex={false}>
          <Text size={Size.h1} center bold>
            Bienvenue sur{'\n'}
            QRCar
          </Text>
        </Block>
        <Block margin={[Size.base * 3, 0]}>
          <Button
            size={Size.h3}
            center
            bold
            color={Variant.white}
            rounded={50}
            height={50}
            bgColor={Variant.primary}
            onPress={() => navigation.navigate('Login')}>
            Se Connecter
          </Button>
          <Button
            margin={[Size.base * 2, 0]}
            center
            bold
            size={Size.h3}
            color={Variant.white}
            rounded={50}
            height={50}
            bgColor={Variant.primaryDark}
            onPress={() => navigation.navigate('Register')}>
            S'Inscrire
          </Button>
        </Block>
        <Block flex>
          <Text size={Size.base} color={Variant.gray} center regular>
            Pour accèder a l'application vous êtes obliger de vous connecter.
          </Text>
        </Block>
      </Block>
    </LayoutBase>
  );
};

export default StarterScreen;

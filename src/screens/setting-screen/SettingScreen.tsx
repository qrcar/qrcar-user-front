// @ts-ignore
import React, {FC} from 'react';
import {Text, View} from 'react-native';
import {Block, Input} from '../../components/element';
import {TunnelLayout} from '../../components/layout';

const SettingScreen: FC = () => {
  return (
    <TunnelLayout statusBar title='Paramètres'>
      <Input placeholder='Rechercher ' />
    </TunnelLayout>
  );
};

export default SettingScreen;

export {default as SettingScreen} from './setting-screen/SettingScreen';
export {default as StarterScreen} from './starter-screen/StarterScreen';
export {default as RegisterScreen} from './register-screen/RegisterScreen';
export {default as LoginScreen} from './login-screen/LoginScreen';
export {default as HistoryScreen} from './history-screen/HistoryScreen';
export {default as DashboardScreen} from './dashboard-screen/DashboardScreen';

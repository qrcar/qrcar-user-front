import faker from 'faker';
import Card from '../types/resources/Card';

export const createCard = (): Card => ({
  id: faker.random.uuid(),
  card_id: faker.random.uuid(),
  user_id: faker.random.uuid(),
});

export const createCards = (count = 3): Array<Card> => Array.from(new Array(count).map(() => createCard()));

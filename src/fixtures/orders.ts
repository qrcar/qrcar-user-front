import faker from 'faker';
import Order, {PaymentMethod, Status} from '../types/resources/Order';

export const createOrder = (): Order => ({
  id: faker.random.uuid(),
  cost: `${faker.random.number}`,
  date: faker.random.date,
  status: Status.canceled,
  paymentMethod: PaymentMethod.creditCard,
  order_num: faker.random.uuid(),
  user_id: faker.random.uuid(),
  card_id: faker.random.uuid(),
  establishment_id: faker.random.uuid(),
});

export const createOrders = (count = 3): Array<Order> => Array.from(new Array(count).map(() => createOrder()));

import faker from 'faker';
import Address from '../types/resources/Address';

export const createAddress = (): Address => ({
  postalCode: '44200',
  city: 'Nantes',
  addressLine_1: `15 Rue du ${faker.random.word()}`,
});

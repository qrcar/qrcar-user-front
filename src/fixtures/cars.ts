import faker from 'faker';
import Car from '../types/resources/Car';

export const createCar = (): Car => ({
  id: faker.random.uuid(),
  user_id: faker.random.uuid(),
  plateRegistration: 'xs-dze5d-dz',
});

export const createCars = (count = 3): Array<Car> => Array.from(new Array(count).map(() => createCar()));

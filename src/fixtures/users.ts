import faker from 'faker';
import User from '../types/resources/User';

export const createUser = (): User => ({
  id: faker.random.uuid(),
  firstname: faker.random.word,
  email: `${faker.random.word()}@gmail.com`,
  lastname: faker.random.word,
});

export const createUsers = (count): Array<User> => Array.from(new Array(count).map(() => createUser()));

import faker from 'faker';
import Establishment from '../types/resources/Establishment';
import {createAddress} from './addresses';

export const createEstablishment = (): Establishment => ({
  id: faker.random.uuid(),
  siret: faker.random.number,
  address: createAddress(),
  phone: 'string',
  name: 'string',
});

export const createEstablishments = (count = 3): Array<Establishment> =>
  Array.from(new Array(count).map(() => createEstablishment()));

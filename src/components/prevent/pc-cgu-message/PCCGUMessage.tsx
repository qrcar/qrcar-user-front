// @ts-ignore
import React, {FC} from 'react';
import {Text as TextR} from 'react-native';
import {Text} from '../../element';
import Variant from '../../../types/Variant';

const PCCGUMessage: FC = () => {
  return (
    <Text>
      En continuant, vous acceptez nos{' '}
      <TextR style={{color: Variant.primary}} onPress={() => console.log()}>
        conditions
      </TextR>{' '}
      ainsi que notre{' '}
      <TextR style={{color: Variant.primary}} onPress={() => console.log()}>
        politique de confidentialité
      </TextR>
    </Text>
  );
};

export default PCCGUMessage;

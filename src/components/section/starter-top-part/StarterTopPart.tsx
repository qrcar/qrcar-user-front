// @ts-ignore
import React, {FC} from 'react';
import Svg, {Path} from 'react-native-svg';
import {Dimensions, Image} from 'react-native';
import {Block} from '../../element';

const StarterTopPart: FC = () => {
  return (
    <Block
      style={{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
      }}
      center>
      <Image
        source={require('../../../assets/images/logo-qrcar.png')}
        style={{
          width: 200,
          height: 200,
          position: 'absolute',
          zIndex: 2,
          top: '25%',
        }}
      />
      <Svg
        height={375}
        width={Dimensions.get('window').width}
        viewBox={`0 0 350 ${Dimensions.get('window').width + 10}`}
        fill='none'>
        <Path d='M-57 -4H429V388.877C233.757 433.995 126.938 437.389 -57 388.877V-4Z' fill='#F0EBF7' />
      </Svg>
    </Block>
  );
};

export default StarterTopPart;

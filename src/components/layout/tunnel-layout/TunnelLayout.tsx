// @ts-ignore
import React, {FC, ReactNode} from 'react';
import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LayoutBase from '../statusbar-layout/LayoutBase';
import {Block, Text} from '../../element';
import Variant from '../../../types/Variant';

export interface Props {
  statusBar?: boolean;
  children: ReactNode;
  back?: boolean;
  title?: string;
}

const TunnelLayout: FC<Props> = ({statusBar, children, title, back}) => {
  const navigation = useNavigation();
  const SPACING = 20;

  return (
    <LayoutBase statusBar={statusBar}>
      <Block height={75} row flex={false} padding={[30, SPACING, 0]} center>
        {back && (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name='home' color={Variant.darkgray} size={20} />
          </TouchableOpacity>
        )}
        <Text bold h1>
          {title}
        </Text>
      </Block>
      <Block flex padding={[0, SPACING]}>
        {children}
      </Block>
    </LayoutBase>
  );
};

export default TunnelLayout;

// @ts-ignore
import React, {FC, ReactNode} from 'react';
import {Platform, StatusBar, View} from 'react-native';
import {Variant} from '../../../types';

export interface Props {
  statusBar?: boolean;
  children: ReactNode;
}

const LayoutBase: FC<Props> = ({statusBar, children}) => {
  return (
    <View style={{backgroundColor: Variant.white, flex: 1}}>
      <View style={[{flex: 1}, statusBar && {marginTop: Platform.OS === 'ios' ? 30 : StatusBar.currentHeight}]}>
        {children}
      </View>
    </View>
  );
};

export default LayoutBase;

// @ts-ignore
import React, {FC, ReactNode} from 'react';
import {StyleProp, StyleSheet, Text, ViewStyle} from 'react-native';
import {ISize, IVariant, Size, Variant} from '../../../types';

export interface Props extends ISize, IVariant {
  children: ReactNode;
  size?: Size | number;
  transform?: boolean;
  align?: boolean;
  regular?: boolean;
  bold?: boolean;
  semibold?: boolean;
  medium?: boolean;
  weight?: number;
  light?: boolean;
  center?: boolean;
  right?: boolean;
  spacing?: boolean;
  height?: boolean;
  color?: Variant;
  style?: StyleProp<ViewStyle>;
}

const Typography: FC<Props> = (props) => {
  const {
    h1,
    h0,
    h2,
    h3,
    title,
    body,
    header,
    caption,
    small,
    size,
    transform,
    align,
    regular,
    bold,
    semibold,
    medium,
    weight,
    light,
    center,
    right,
    spacing, // letter-spacing
    height, // line-height
    // colors
    color,
    accent,
    primary,
    secondary,
    tertiary,
    black,
    white,
    gray,
    gray2,
    gray3,
    children,
    style,
  } = props;

  const textStyles = [
    styles.text,
    h0 && {fontSize: Size.h0},
    h1 && {fontSize: Size.h1},
    h2 && {fontSize: Size.h2},
    h3 && {fontSize: Size.h3},
    title && {fontSize: Size.title},
    header && {fontSize: Size.header},
    body && {fontSize: Size.body},
    caption && {fontSize: Size.caption},
    small && {fontSize: Size.small},
    size && {fontSize: size},
    transform && {textTransform: transform},
    align && {textAlign: align},
    height && {lineHeight: height},
    spacing && {letterSpacing: spacing},
    weight && {fontWeight: weight},
    regular && styles.regular,
    bold && styles.bold,
    semibold && styles.semibold,
    medium && styles.medium,
    light && styles.light,
    center && styles.center,
    right && styles.right,
    color && styles[color],
    color && !styles[color] && {color},
    // color shortcuts
    accent && {color: Variant.accent},
    primary && {color: Variant.primary},
    secondary && {color: Variant.secondary},
    tertiary && {color: Variant.tertiary},
    black && {color: Variant.black},
    white && {color: Variant.white},
    gray && {color: Variant.gray},
    gray2 && {color: Variant.gray2},
    gray3 && {color: Variant.gray3},
    style,
  ];

  return <Text style={textStyles}>{children}</Text>;
};

export default Typography;

const styles = StyleSheet.create({
  // default style
  text: {
    fontSize: Size.font,
    color: Variant.black,
  },
  // variations
  regular: {
    fontWeight: 'normal',
  },
  bold: {
    fontWeight: 'bold',
  },
  semibold: {
    fontWeight: '500',
  },
  medium: {
    fontWeight: '500',
  },
  light: {
    fontWeight: '200',
  },
  // position
  center: {textAlign: 'center'},
  right: {textAlign: 'right'},
});

// @ts-ignore
import React, {FC, ReactNode} from 'react';
import {TouchableOpacity, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import {Block, Text} from '../index';
import {ISize, IVariant, Variant} from '../../../types';
import Size from '../../../types/Size';

export interface Props extends ISize, IVariant {
  children?: ReactNode;
  opacity?: number;
  style?: StyleProp<ViewStyle>;
  large?: boolean;
  bgColor?: Variant;
  shadow?: boolean;
  small?: boolean;
  medium?: boolean;
  rounded?: number;
  height?: number;
  regular?: boolean;
  bold?: boolean;
  semibold?: boolean;
  mediumT?: boolean;
  weight?: number;
  light?: boolean;
  center?: boolean;
  color?: Variant;
  margin?: number | Array<number>;
  size?: number | Size;
  onPress?: () => void;
}

const ButtonBase: FC<Props> = ({
  children,
  opacity,
  small,
  large,
  height,
  rounded,
  medium,
  shadow,
  bgColor,
  style,
  regular,
  bold,
  semibold,
  weight,
  mediumT,
  light,
  center,
  color,
  margin,
  size,
  onPress,
}) => {
  const buttonStyles = [
    styles.button,
    height && {height},
    rounded && {borderRadius: rounded},
    medium && styles.medium,
    small && styles.small,
    large && styles.large,
    shadow && styles.shadow,
    bgColor && styles[bgColor], // predefined styles colors for backgroundColor
    bgColor && !styles[bgColor] && {backgroundColor: bgColor}, // custom backgroundColor
    style,
  ];

  return (
    <Block flex={false} margin={margin}>
      <TouchableOpacity style={buttonStyles} activeOpacity={opacity || 0.8} onPress={onPress}>
        <Text
          color={color}
          regular={regular}
          center={center}
          light={light}
          semibold={semibold}
          weight={weight}
          bold={bold}
          medium={mediumT}
          size={size}>
          {children}
        </Text>
      </TouchableOpacity>
    </Block>
  );
};

export default ButtonBase;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
  },
  shadow: {
    shadowColor: Variant.black,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },
  medium: {
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
  },
  large: {
    paddingTop: 1,
    paddingRight: 1,
    paddingBottom: 1,
    paddingLeft: 1,
  },
  small: {
    paddingTop: 1,
    paddingRight: 1,
    paddingBottom: 1,
    paddingLeft: 1,
  },
  rounded: {
    borderRadius: 360,
  },
});

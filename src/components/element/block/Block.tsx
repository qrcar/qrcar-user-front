// @ts-ignore
import React, {FC, ReactNode} from 'react';
import {StyleSheet, View, Animated, FlexStyle, StyleProp, ViewStyle} from 'react-native';
import {Size, Variant} from '../../../types';

export interface Props extends Omit<FlexStyle, 'margin' | 'padding' | 'flex'> {
  margin?: number | Array<number>;
  padding?: number | Array<number>;
  animated?: boolean;
  children?: ReactNode;
  card?: boolean;
  row?: boolean;
  column?: boolean;
  center?: boolean;
  middle?: boolean;
  flex?: boolean;
  space?: 'start' | 'end' | 'center' | 'between' | 'around' | 'evenly';
  wrap?: boolean;
  shadow?: boolean;
  color?: Variant;
  style?: StyleProp<ViewStyle>;
}

const Block: FC<Props> = ({
  style,
  space,
  wrap,
  card,
  flex,
  row,
  color,
  column,
  center,
  middle,
  margin,
  children,
  animated,
  padding,
  shadow,
  bottom,
  left,
  right,
  top,
  height,
  width,
}) => {
  // eslint-disable-next-line consistent-return
  const handleMargins = () => {
    if (typeof margin === 'number') {
      return {
        marginTop: margin,
        marginRight: margin,
        marginBottom: margin,
        marginLeft: margin,
      };
    }

    if (typeof margin === 'object') {
      const marginSize = margin.length;
      switch (marginSize) {
        case 1:
          return {
            marginTop: margin[0],
            marginRight: margin[0],
            marginBottom: margin[0],
            marginLeft: margin[0],
          };
        case 2:
          return {
            marginTop: margin[0],
            marginRight: margin[1],
            marginBottom: margin[0],
            marginLeft: margin[1],
          };
        case 3:
          return {
            marginTop: margin[0],
            marginRight: margin[1],
            marginBottom: margin[2],
            marginLeft: margin[1],
          };
        default:
          return {
            marginTop: margin[0],
            marginRight: margin[1],
            marginBottom: margin[2],
            marginLeft: margin[3],
          };
      }
    }
  };

  // eslint-disable-next-line consistent-return
  const handlePaddings = () => {
    if (typeof padding === 'number') {
      return {
        paddingTop: padding,
        paddingRight: padding,
        paddingBottom: padding,
        paddingLeft: padding,
      };
    }

    if (typeof padding === 'object') {
      const paddingSize = padding.length;
      switch (paddingSize) {
        case 1:
          return {
            paddingTop: padding[0],
            paddingRight: padding[0],
            paddingBottom: padding[0],
            paddingLeft: padding[0],
          };
        case 2:
          return {
            paddingTop: padding[0],
            paddingRight: padding[1],
            paddingBottom: padding[0],
            paddingLeft: padding[1],
          };
        case 3:
          return {
            paddingTop: padding[0],
            paddingRight: padding[1],
            paddingBottom: padding[2],
            paddingLeft: padding[1],
          };
        default:
          return {
            paddingTop: padding[0],
            paddingRight: padding[1],
            paddingBottom: padding[2],
            paddingLeft: padding[3],
          };
      }
    }
  };
  const blockStyles = [
    styles.block,
    height && {height},
    width && {width},
    flex && {flex},
    flex === false && {flex: 0}, // reset / disable flex
    row && styles.row,
    column && styles.column,
    center && styles.center,
    middle && styles.middle,
    left && styles.left,
    right && styles.right,
    top && styles.top,
    bottom && styles.bottom,
    margin && {...handleMargins()},
    padding && {...handlePaddings()},
    card && styles.card,
    shadow && styles.shadow,
    space && {justifyContent: `space-${space}`},
    wrap && {flexWrap: 'wrap'},
    color && styles[color], // predefined styles colors for backgroundColor
    color && !styles[color] && {backgroundColor: color}, // custom backgroundColor
    style, // rewrite predefined styles
  ];

  if (animated) {
    return <Animated.View style={blockStyles}>{children}</Animated.View>;
  }

  return <View style={blockStyles}>{children}</View>;
};

export default Block;

export const styles = StyleSheet.create({
  block: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  card: {
    borderRadius: Size.radius,
  },
  center: {
    alignItems: 'center',
  },
  middle: {
    justifyContent: 'center',
  },
  left: {
    justifyContent: 'flex-start',
  },
  right: {
    justifyContent: 'flex-end',
  },
  top: {
    justifyContent: 'flex-start',
  },
  bottom: {
    justifyContent: 'flex-end',
  },
  shadow: {
    shadowColor: Variant.black,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 13,
    elevation: 2,
  },
  accent: {backgroundColor: Variant.accent},
  primary: {backgroundColor: Variant.primary},
  secondary: {backgroundColor: Variant.secondary},
  tertiary: {backgroundColor: Variant.tertiary},
  black: {backgroundColor: Variant.black},
  white: {backgroundColor: Variant.white},
  gray: {backgroundColor: Variant.gray},
  gray2: {backgroundColor: Variant.gray2},
});

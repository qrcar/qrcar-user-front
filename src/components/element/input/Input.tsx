// @ts-ignore
import React, {FC, useState} from 'react';
import {TextInput, View, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import {Size, Variant} from '../../../types';
import {Block} from '../index';

export interface Props {
  onChange?: (value: string) => void;
  value?: string;
  style?: StyleProp<ViewStyle>;
  error?: boolean;
  secure?: boolean;
  type?: 'email-address' | 'numeric' | 'default' | 'phone-pad';
  placeholder?: string;
}

const Input: FC<Props> = ({value, onChange, placeholder, type = 'default', style, secure, error}) => {
  const [toggleSecure, setToggleSecure] = useState<boolean>(false);
  const inputStyles = [styles.input, error && {borderColor: Variant.accent}, style];
  const isSecure = toggleSecure ? false : secure;

  return (
    <Block flex={false} margin={[10, 0]}>
      <TextInput
        placeholder={placeholder}
        style={inputStyles}
        secureTextEntry={isSecure}
        //  autoComplete='off'
        autoCapitalize='none'
        keyboardType={type}
        value={value}
        onChangeText={(text) => onChange(text)}
      />
    </Block>
  );
};

export default Input;

const styles = StyleSheet.create({
  input: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Variant.gray3,
    borderRadius: Size.radius,
    fontSize: Size.font,
    fontWeight: '500',
    color: Variant.black,
    height: Size.base * 3,
    backgroundColor: Variant.gray3,
    paddingLeft: 10,
  },
  toggle: {
    position: 'absolute',
    alignItems: 'flex-end',
    width: Size.base * 2,
    height: Size.base * 2,
    top: Size.base,
    right: 0,
  },
});

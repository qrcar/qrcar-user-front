// @ts-ignore
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import {DashboardScreen, HistoryScreen, SettingScreen} from '../../screens';
import {Variant} from '../../types';

const Tabs = createBottomTabNavigator();

const MainNavigator = function NavTabs() {
  return (
    <Tabs.Navigator
      tabBarOptions={{
        labelStyle: {fontSize: 13, fontWeight: 'bold'},
        activeTintColor: Variant.primary,
        inactiveTintColor: 'black',
      }}>
      <Tabs.Screen
        name='Home'
        component={DashboardScreen}
        options={{
          title: 'Accueil',
          tabBarIcon: ({color, size}) => <Icon name='home' color={color} size={size} />,
        }}
      />
      <Tabs.Screen
        name='History'
        component={HistoryScreen}
        options={{
          title: 'Historique',
          tabBarIcon: ({color, size}) => <Icon name='history' color={color} size={size} />,
        }}
      />
      <Tabs.Screen
        name='Setting'
        component={SettingScreen}
        options={{
          title: 'Paramètres',
          tabBarIcon: ({color, size}) => <Icon name='gear' color={color} size={size} />,
        }}
      />
    </Tabs.Navigator>
  );
};

export default MainNavigator;

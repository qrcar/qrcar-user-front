// @ts-ignore
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {StarterScreen, LoginScreen, RegisterScreen} from '../../screens';

const Stack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator initialRouteName='Starter' headerMode='none'>
      <Stack.Screen name='Starter' component={StarterScreen} options={{cardStyle: {backgroundColor: 'white'}}} />
      <Stack.Screen name='Login' component={LoginScreen} options={{cardStyle: {backgroundColor: 'white'}}} />
      <Stack.Screen name='Register' component={RegisterScreen} options={{cardStyle: {backgroundColor: 'white'}}} />
    </Stack.Navigator>
  );
};

export default AuthNavigator;

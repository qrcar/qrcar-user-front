// @ts-ignore
import React, {FC} from 'react';
import {Alert} from 'react-native';
import {Block, Button, Input, Text} from '../../element';
import useFormInput from '../../../hooks/formInputHook';
import Size from '../../../types/Size';
import Variant from '../../../types/Variant';
import {PCCGUMessage} from '../../prevent';

const LoginForm: FC = () => {
  const form = {
    email: useFormInput<string>(''),
    password: useFormInput<string>(''),
  };

  const handleLogin = () => {
    Alert.alert('value', `${JSON.stringify(form)}`);
  };

  return (
    <Block flex={false} margin={[10, 0]}>
      <Input placeholder='Email' {...form.email} />
      <Input placeholder='Mot de passe' secure {...form.password} />
      <Block flex={false} margin={[20, 50, 30, 0]}>
        <PCCGUMessage />
      </Block>
      <Button
        size={Size.h3}
        center
        bold
        color={Variant.white}
        rounded={50}
        height={50}
        bgColor={Variant.primary}
        onPress={() => handleLogin()}>
        Se connecter
      </Button>
    </Block>
  );
};

export default LoginForm;

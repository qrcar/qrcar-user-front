// @ts-ignore
import React, {FC} from 'react';
import {Alert, Text as TextR} from 'react-native';
import {Block, Button, Input, Text} from '../../element';
import useFormInput from '../../../hooks/formInputHook';
import {Size} from '../../../types';
import Variant from '../../../types/Variant';
import {PCCGUMessage} from '../../prevent';

const RegisterForm: FC = () => {
  const form = {
    lastname: useFormInput<string>(''),
    firstname: useFormInput<string>(''),
    email: useFormInput<string>(''),
    password: useFormInput<string>(''),
  };

  const handleRegister = () => {
    Alert.alert('value', `${JSON.stringify(form)}`);
  };

  return (
    <Block margin={[10, 0]}>
      <Input placeholder='Nom' {...form.lastname} />
      <Input placeholder='Prénom' {...form.firstname} />
      <Input placeholder='Email' {...form.email} />
      <Input placeholder='Mot de passe' secure {...form.password} />
      <Block flex={false} margin={[20, 50, 30, 0]}>
        <PCCGUMessage />
      </Block>
      <Button
        size={Size.h3}
        center
        bold
        color={Variant.white}
        rounded={50}
        height={50}
        bgColor={Variant.primary}
        onPress={() => handleRegister()}>
        Se connecter
      </Button>
    </Block>
  );
};

export default RegisterForm;

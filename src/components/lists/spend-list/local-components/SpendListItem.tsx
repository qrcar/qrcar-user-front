// @ts-ignore
import React, {FC} from 'react';
import {Block, Text} from '../../../element';
import Size from '../../../../types/Size';
import Variant from '../../../../types/Variant';

export interface Props {
  item: any;
}

const SpendListItem: FC<Props> = ({item}) => {
  return (
    <Block flex={false} row space='between' center padding={[Size.base, 0]}>
      <Block flex={false}>
        <Text size={Size.caption} color={Variant.darkgray}>
          Lundi 9 Novembre
        </Text>
        <Text size={Size.base} color={Variant.black} style={{marginTop: 5}}>
          Parking Feydeau
        </Text>
      </Block>
      <Block flex={false}>
        <Text bold size={Size.title}>
          24,18 €
        </Text>
      </Block>
    </Block>
  );
};

export default SpendListItem;

// @ts-ignore
import React, {FC} from 'react';
import SpendListItem from './local-components/SpendListItem';

export interface Props {
  spends: any;
}

const SpendList: FC<Props> = ({spends}) => {
  // eslint-disable-next-line react/no-array-index-key
  return spends.map((spend, index) => <SpendListItem key={index} item={spend} />);
};

export default SpendList;

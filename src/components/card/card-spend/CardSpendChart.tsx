// @ts-ignore
import React, {FC, ReactNode} from 'react';
import {VictoryBar, VictoryChart, VictoryAxis, VictoryTheme, VictoryLabel} from 'victory-native';
import {Block, Text} from '../../element';
import Variant from '../../../types/Variant';
import Size from '../../../types/Size';

export interface Props {
  children?: ReactNode;
  title?: string;
  flex?: boolean;
}

const CardSpendChart: FC<Props> = ({children, flex = true, title = 'Dépenses ce mois-ci'}) => {
  const data = [
    {quarter: 1, earnings: 328},
    {quarter: 2, earnings: 156},
    {quarter: 3, earnings: 249},
    {quarter: 4, earnings: 167},
    {quarter: 5, earnings: 508},
    {quarter: 6, earnings: 124},
  ];

  return (
    <Block flex={false} height={220}>
      <Text color={Variant.gray}>{title}</Text>
      <Block center>
        <VictoryChart height={200}>
          <VictoryAxis
            tickValues={[1, 2, 3, 4, 5, 6]}
            tickFormat={['janv', 'fev', 'mar', 'avr', 'mai', 'juin']}
            style={{
              axis: {display: 'none'},
              axisLabel: {color: Variant.gray},
              tickLabels: {color: Variant.gray},
            }}
          />
          <VictoryBar
            labels={({datum}) => datum.y}
            data={data}
            x='quarter'
            y='earnings'
            cornerRadius={{top: 8, bottom: 8}}
            style={{
              data: {
                fill: Variant.primary,
                filter: 'dropShadow(0px 3px 10px rgba(164, 118, 239, 0.4))',
                width: 35,
              },
            }}
          />
        </VictoryChart>
      </Block>
    </Block>
  );
};

export default CardSpendChart;

// @ts-ignore
import React, {FC, ReactNode} from 'react';
import {Block, Text} from '../../element';
import Variant from '../../../types/Variant';
import Size from '../../../types/Size';

export interface Props {
  children?: ReactNode;
  title?: string;
  value: number;
  flex?: boolean;
}

const CardSpend: FC<Props> = ({children, flex = true, title = 'Dépenses ce mois-ci', value}) => {
  return (
    <Block flex={false} height={150}>
      <Text color={Variant.gray}>{title}</Text>
      <Block flex middle center>
        <Text color={Variant.black} bold size={Size.title * 2.5}>
          {value} €
        </Text>
      </Block>
    </Block>
  );
};

export default CardSpend;

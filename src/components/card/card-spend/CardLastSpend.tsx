// @ts-ignore
import React, {FC} from 'react';
import {Block, Text} from '../../element';
import Size from '../../../types/Size';
import Variant from '../../../types/Variant';
import {SpendList} from '../../lists';

export interface Props {
  title: string;
}

const CardLastSpend: FC<Props> = ({title}) => {
  const spends = [{}, {}, {}, {}];

  return (
    <Block flex>
      <Text color={Variant.gray}>{title}</Text>
      {spends.length > 0 ? (
        <Block margin={[10, 0]}>
          <SpendList spends={spends} />
        </Block>
      ) : (
        <Block middle>
          <Text center size={Size.title} color={Variant.gray2} semibold>
            Vous n'avez éffectué aucune{'\n'}dépense pour le moment
          </Text>
        </Block>
      )}
    </Block>
  );
};

export default CardLastSpend;

import {useState} from 'react';

export interface InputStateHandler<T> {
  value: T;
  onChange: (newValue: T) => void;
}

/**
 * React hook for managing input states.
 */
function useFormInput<T>(initialValue: T): InputStateHandler<T> {
  const [value, setValue] = useState(initialValue);
  return {value, onChange: setValue};
}

export default useFormInput;
